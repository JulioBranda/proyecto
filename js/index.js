<script>
         $(function(){
          $("[data-toggle='tooltip']").tooltip();
          $("[data-toggle='popover']").popover();
          $(".carousel").carousel({interval: 1500});

          $("#termsYconds").on("show.bs.modal", function (e){
            console.log("El modal de contacto se está mostrando");
            $("#tycBTN").removeClass("btn-dark");
            $("#tycBTN").addClass("btn-outline-secondary");
            $("#tycBTN").prop("disabled", true);
          });
          $("#termsYconds").on("shown.bs.modal", function (e){
            console.log("El modal de contacto se mostró");
          });
          $("#termsYconds").on("hide.bs.modal", function (e){
            console.log("El modal de contacto se oculta");
          });
          $("#termsYconds").on("hidden.bs.modal", function (e){
            console.log("El modal de contacto se ocultó");
            $("#tycBTN").removeClass("btn-outline-secondary");
            $("#tycBTN").addClass("btn-dark");
            $("#tycBTN").prop("disabled", false);
          });
        });

</script>
